﻿using UnityEngine;

namespace Maru
{
    /// <summary>
    /// グリッドベースの移動を行うクラス
    /// </summary>
    [RequireComponent( typeof( PhotonView ), typeof( PhotonTransformView ) )]
    public class GridBasedMover : Photon.MonoBehaviour
    {
        #region Types
        public delegate void OnMoveAcrossGridCallback( Vector3 nextPosition );
        #endregion

        #region Constants
        const float MovingSpeed = 8.0f;
        #endregion

        #region Variables
        #endregion

        #region Properties
        public Vector3 Direction { get; set; }
        public Vector3 CurrentGridPosition { get; set; }
        public OnMoveAcrossGridCallback OnMoveAcrossGrid { get; set; }
        #endregion

        #region Messages
        //----------------------------------------------------------------------
        void Awake()
        {
            Direction = transform.forward;
        }

        //----------------------------------------------------------------------
        void Update()
        {
            if (!photonView.isMine)
            {
                return;
            }

            // Time.deltaTime が大きい場合のすり抜け防止
            const float MaxDeltaTime = 0.05f;
            var times = (int)(Time.deltaTime / MaxDeltaTime) + 1;
            var adjustedDeltaTime = Time.deltaTime / times;

            for (int i = 0; i < times; i++)
            {
                // 次に移動する位置の算出
                var nextPosition = transform.position;
                nextPosition += Direction * MovingSpeed * adjustedDeltaTime;

                // 1 番近いグリッドが現在居るグリッドの位置となる
                var nearGridPosition = new Vector3(
                    Mathf.Round( nextPosition.x ),
                    nextPosition.y,
                    Mathf.Round( nextPosition.z )
                );
                CurrentGridPosition = nearGridPosition;

                // 小数点以下を切り捨てれば現在居るグリッドの位置が求まる
                var currentGridXPosition = (int)transform.position.x;
                var currentGridZPosition = (int)transform.position.z;
                var nextGridXPosition = (int)nextPosition.x;
                var nextGridZPosition = (int)nextPosition.z;

                // グリッドの境界を通過した？
                if ((currentGridXPosition != nextGridXPosition)
                    || (currentGridZPosition != nextGridZPosition))
                {
                    OnMoveAcrossGrid( nextPosition );
                }

                // 壁と衝突した？
                const float HalfGridSize = 0.5f;
                var layerMask = LayerMask.GetMask( "Wall" );
                if (Physics.Raycast( new Ray( transform.position, Direction ), HalfGridSize, layerMask ))
                {
                    // 押し戻す
                    nextPosition = nearGridPosition;
                    // グリッドの境界を通過したとみなす
                    OnMoveAcrossGrid( nextPosition );
                }

                // 実際に位置の更新
                transform.position = nextPosition;
            }

            PhotonNetwork.player.SetPosition( transform.position );
        }
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        /// <summary>
        /// 指定した方向に移動できる？
        /// </summary>
        /// <param name="direction">移動をテストしたい方向</param>
        /// <returns>移動できるか否か</returns>
        public bool IsMovableTo( Vector3 direction )
        {
            var movedPosition = CurrentGridPosition + direction;

            return MapManager.Instance.GetDataFromCurrentMap( movedPosition ) != MapElements.Wall;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// 指定した方向は進行方向と比較して逆向き？
        /// </summary>
        /// <param name="targetDirection">後ろ向きか否かをテストしたい方向</param>
        /// <returns>後ろ向きか否か</returns>
        public bool IsBackwardDirection( Vector3 targetDirection )
        {
            const float Threshold = 0.0001f;

            return Vector3.Dot( targetDirection, Direction ) < -1.0f + Threshold;
        }
        #endregion
    }
}
