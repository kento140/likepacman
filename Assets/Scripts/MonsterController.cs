﻿using UnityEngine;

namespace Maru
{
    /// <summary>
    /// モンスターの制御を行うクラス
    /// </summary>
    [RequireComponent( typeof( GridBasedMover ) )]
    public class MonsterController : MonoBehaviour
    {
        #region Constants
        #endregion

        #region Variables
        #endregion

        #region Properties
        InputHandler.Type HandleInput { get; set; }
        #endregion

        #region Messages
        //----------------------------------------------------------------------
        void OnEnable()
        {
            CreatePlayableMonster( this );

            var gridBasedMover = GetComponent<GridBasedMover>();

            gridBasedMover.OnMoveAcrossGrid =
                nextPosition =>
                {
                    var direction = HandleInput();

                    // キー入力がない？
                    if (direction == Vector3.zero)
                    {
                        return;
                    }
                    // 移動方向が壁？
                    if (!gridBasedMover.IsMovableTo( direction ))
                    {
                        return;
                    }
                    // 後ろ方向へは移動できない
                    //if (gridBasedMover.IsBackwardDirection( direction ))
                    //{
                    //    return;
                    //}

                    // 実際に移動
                    gridBasedMover.Direction = direction;
                };
        }
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        public static void CreatePlayableMonster( MonsterController target )
        {
            target.GetComponent<Renderer>().material.color = Color.magenta;

            target.HandleInput = InputHandler.GetPlayable();

        }

        //----------------------------------------------------------------------
        public static void CreateNonPlayableMonster( MonsterController target )
        {
            target.GetComponent<Renderer>().material.color = Color.red;

            target.HandleInput = InputHandler.GetNonPlayable( target );
        }
        #endregion
    }
}
