﻿using PhotonRx;
using UniRx;
using UnityEngine;

namespace Maru
{
    [RequireComponent( typeof( PhotonView ) )]
    public class RoomScene : Photon.MonoBehaviour
    {
        #region Constants
        #endregion

        #region Variables
        string userName = "名前を入力して下さい";
        #endregion

        #region Properties
        #endregion

        #region Messages
        //----------------------------------------------------------------------
        void Awake()
        {
            // Photon のメッセージを購読 ...
            // ... ロビーに入室したとき
            this.OnJoinedLobbyAsObservable()
                .Subscribe( _ =>
                {
                    Debug.Log( string.Format( "OnOnJoinedLobby(): {0}", PhotonNetwork.player.ID ) );

                    // とりあえずどこかのルームへ入室する
                    PhotonNetwork.JoinRandomRoom();
                } );

            // ... ルームに入室したとき
            this.OnJoinedRoomAsObservable()
                .Subscribe( _ =>
                {
                    Debug.Log( string.Format( "OnOnJoinedRoom(): {0}", PhotonNetwork.player.ID ) );

                    // ルームの既存プレイヤはシーン遷移済み？
                    object loaded;
                    if (PhotonNetwork.room.customProperties.TryGetValue( "Loaded", out loaded )
                        && (bool)loaded)
                    {
                        // 既存プレイヤに続いてシーン遷移する
                        ReplaceScene();
                    }
                } );

            // ... ルームの入室に失敗したとき
            this.OnPhotonRandomJoinFailedAsObservable()
                // 自分でルームを作成して入室
                .Subscribe( _ => PhotonNetwork.CreateRoom( null ) );
        }

        //----------------------------------------------------------------------
        void Start()
        {
        }

        //----------------------------------------------------------------------
        void Update()
        {
        }

        //----------------------------------------------------------------------
        void OnGUI()
        {
            if (!PhotonNetwork.inRoom)
            {
                userName = GUI.TextField( new Rect( 0, 0, 200, 50 ), userName, 10 );

                if (GUI.Button( new Rect( 0, 50, 100, 100 ), "決定" ))
                {
                    PhotonNetwork.playerName = userName;

                    // サーバへ接続してロビーへ入室
                    PhotonNetwork.ConnectUsingSettings( null );
                }

                return;
            }

            GUI.Label(
                new Rect( 0, 0, 100, 50 ),
                string.Format( "現在 {0} 人", PhotonNetwork.playerList.Length )
            );

            if (!PhotonNetwork.isMasterClient)
            {
                return;
            }

            if (GUI.Button( new Rect( 0, 50, 100, 100 ), "ゲーム開始" ))
            {
                // 一時的に新規の入室を禁止する
                PhotonNetwork.room.open = false;
                // シーン遷移する際には呼ばないとエラーが起きる
                PhotonNetwork.DestroyAll();

                // 全クライアントのシーン遷移
                photonView.RPC( "ReplaceScene", PhotonTargets.All );
            }
        }
        #endregion

        #region RPC
        //----------------------------------------------------------------------
        [PunRPC]
        void ReplaceScene()
        {
            // 一時的にイベントの送受信を遮断
            PhotonNetwork.isMessageQueueRunning = false;

            UnityEngine.SceneManagement.SceneManager.LoadScene( "Play" );
        }
        #endregion

        #region Methods
        #endregion
    }
}
