﻿using UnityEngine;

namespace Maru
{
    /// <summary>
    /// Input のラッパークラス
    /// </summary>
    static public class GameInput
    {
        #region Properties
        public static bool Up
        {
            get
            {
                return Input.GetKey( KeyCode.UpArrow )
                    || Input.GetKey( KeyCode.W );
            }
        }
        public static bool Down
        {
            get
            {
                return Input.GetKey( KeyCode.DownArrow )
                    || Input.GetKey( KeyCode.S );
            }
        }
        public static bool Right
        {
            get
            {
                return Input.GetKey( KeyCode.RightArrow )
                    || Input.GetKey( KeyCode.D );
            }
        }
        public static bool Left
        {
            get
            {
                return Input.GetKey( KeyCode.LeftArrow )
                    || Input.GetKey( KeyCode.A );
            }
        }
        #endregion
    }
}
