﻿using UnityEngine;
using ExitGames.Client.Photon;

namespace Maru
{
    public static class PhotonPlayerExtensions
    {
        #region Constants
        const string PositionKey = "Position";
        const string IsPlayerKey = "IsPlayer";
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        public static void SetPosition( this PhotonPlayer self, Vector3 value )
        {
            var position = new Hashtable();
            position[PositionKey] = value;

            self.SetCustomProperties( position );
        }

        //----------------------------------------------------------------------
        public static Vector3 GetPosition( this PhotonPlayer self )
        {
            object value;
            if (self.customProperties.TryGetValue( PositionKey, out value ))
            {
                return (Vector3)value;
            }

            return Vector3.zero;
        }

        //----------------------------------------------------------------------
        public static void IsPlayer( this PhotonPlayer self, bool value )
        {
            var isPlayer = new Hashtable();
            isPlayer[IsPlayerKey] = value;

            self.SetCustomProperties( isPlayer );
        }

        //----------------------------------------------------------------------
        public static bool IsPlayer( this PhotonPlayer self )
        {
            object value;
            if (self.customProperties.TryGetValue( IsPlayerKey, out value ))
            {
                return (bool)value;
            }

            return false;
        }
        #endregion
    }
}
