﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Maru
{
    /// <summary>
    /// プレイヤの制御を行うクラス
    /// </summary>
    [RequireComponent( typeof( GridBasedMover ) )]
    public class PlayerController : Photon.MonoBehaviour
    {
        #region Constants
        #endregion

        #region Variables
        GridBasedMover gridBasedMover = null;
        #endregion

        #region Properties
        InputHandler.Type HandleInput { get; set; }
        #endregion

        #region Messages
        //----------------------------------------------------------------------
        void OnEnable()
        {
            CreatePlayablePlayer( this );

            gridBasedMover = GetComponent<GridBasedMover>();

            gridBasedMover.OnMoveAcrossGrid =
                nextPosition =>
                {
                    // エサを食べる
                    MapManager.Instance.NotifyRemovingFood( nextPosition );

                    var direction = HandleInput();

                    // キー入力がない？
                    if (direction == Vector3.zero)
                    {
                        return;
                    }

                    // 'direction' 方向へ移動可能でない？
                    if (!gridBasedMover.IsMovableTo( direction ))
                    {
                        return;
                    }

                    // 実際に移動
                    gridBasedMover.Direction = direction;
                };
        }

        //----------------------------------------------------------------------
        void Update()
        {
            if (!photonView.isMine)
            {
                return;
            }

            var direction = HandleInput();

            // キー入力がない？
            if (direction == Vector3.zero)
            {
                return;
            }

            // 'direction' は後ろ方向？
            if (gridBasedMover.IsBackwardDirection( direction ))
            {
                // 後ろ方向へはいつでも移動可能
                gridBasedMover.Direction = direction;
            }
        }

        //----------------------------------------------------------------------
        void OnTriggerEnter( Collider alwaysMonster )
        {
            if (!photonView.isMine)
            {
                return;
            }

            // コンポーネントが無効な状態でも呼ばれてしまうため
            if (!enabled)
            {
                return;
            }

            var room = PhotonNetwork.room;

            // 無敵処理
            if (room.GetInvincibleTime() > 0.0f)
            {
                return;
            }
            const float InvincibleTime = 1.0f;
            room.SetInvincibleTime( InvincibleTime );

            // スコア処理
            room.SetCurrentScore( 0 );
            room.SetCurrentPlayer( PhotonView.Get( alwaysMonster ).owner.name );

            // プレイヤとモンスターの入れ替え
            photonView.RPC(
                "SwapControllers",
                PhotonTargets.AllBuffered,
                new object[] { PhotonNetwork.player.ID, PhotonView.Get( alwaysMonster ).owner.ID }
            );
        }
        #endregion

        #region RPC
        //----------------------------------------------------------------------
        [PunRPC]
        void SwapControllers( int playerID, int monsterID )
        {
            var player = GameObject.FindObjectsOfType<PlayerController>()
                .Select( e => e.gameObject )
                .Where( e => PhotonView.Get( e ).owner.ID == playerID )
                .First();

            var monster = GameObject.FindObjectsOfType<MonsterController>()
                .Select( e => e.gameObject )
                .Where( e => PhotonView.Get( e ).owner.ID == monsterID )
                .First();

            // プレイヤをモンスターに
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<MonsterController>().enabled = true;
            // モンスターをプレイヤに
            monster.GetComponent<MonsterController>().enabled = false;
            monster.GetComponent<PlayerController>().enabled = true;
        }
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        public static void CreatePlayablePlayer( PlayerController target )
        {
            target.GetComponent<Renderer>().material.color = Color.blue;

            target.HandleInput = InputHandler.GetPlayable();
        }

        //----------------------------------------------------------------------
        public static void CreateNonPlayablePlayer( PlayerController target )
        {
            target.GetComponent<Renderer>().material.color = Color.blue;

            target.HandleInput = InputHandler.GetNonPlayable( target );
        }
        #endregion
    }
}
