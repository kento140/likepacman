﻿using UnityEngine;
using System.Collections.Generic;
using UniRx;

namespace Maru
{
    /// <summary>
    /// マップの管理を行うクラス
    /// </summary>
    [RequireComponent( typeof( PhotonView ) )]
    public class MapManager : Photon.MonoBehaviour
    {
        #region Constants
        #endregion

        #region Variables
        [SerializeField]
        TextAsset[] mapFiles = null;

        Vector3 playerPosition = Vector3.zero;
        List<Vector3> monsterPositions = new List<Vector3>();
        // 取り除く必要があるため
        Dictionary<Vector2, GameObject> foods = new Dictionary<Vector2, GameObject>();

        int mapWidth  = 0;
        int mapHeight = 0;
        int[,] currentMapData;
        #endregion

        #region Properties
        public static MapManager Instance { get; private set; }

        public int FoodCount { get; private set; }
        #endregion

        #region Messages
        //----------------------------------------------------------------------
        void Awake()
        {
            Instance = this;
        }

        //----------------------------------------------------------------------
        void Update()
        {
        }
        #endregion

        #region RPC
        //----------------------------------------------------------------------
        [PunRPC]
        void RemoveFoodRPC( Vector3 position )
        {
            var key = new Vector2(
                Mathf.Round( position.x ),
                Mathf.Round( position.z )
            );

            if (!foods.ContainsKey( key ))
            {
                return;
            }

            FoodCount--;
            PhotonNetwork.Destroy( foods[key] );
            foods.Remove( key );

            // スコア処理
            var room = PhotonNetwork.room;

            room.AddCurrentScore( 1 );
            if (room.GetCurrentScore() > room.GetHighScore())
            {
                room.SetHighPlayer( room.GetCurrentPlayer() );
                room.SetHighScore( room.GetCurrentScore() );
            }
        }
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        public void CreateLoadedMap()
        {
            var wallPrefab = Resources.Load<GameObject>( "Prefabs/Wall" );
            //var foodPrefab = Resources.Load<GameObject>( "Prefabs/Food" );

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    var spawnPosition = new Vector3( x, 0, y );

                    if (currentMapData[y, x] == MapElements.Wall)
                    {
                        var wall = Instantiate( wallPrefab, spawnPosition, Quaternion.identity ) as GameObject;
                        wall.GetComponent<Renderer>().material.color = Color.black;
                    }
                    else if (currentMapData[y, x] == MapElements.Player)
                    {
                        playerPosition = spawnPosition;
                    }
                    else if (currentMapData[y, x] == MapElements.Monster)
                    {
                        monsterPositions.Add( spawnPosition );
                    }
                }
            }
        }

        //----------------------------------------------------------------------        
        public int ChooseMap()
        {
            return Random.Range( 0, mapFiles.Length );
        }

        //----------------------------------------------------------------------
        public void LoadMapFromFile( int mapFileIndex )
        {
            var mapData = mapFiles[mapFileIndex].text;

            // 空の要素を削除
            var removeEmptyEntries = System.StringSplitOptions.RemoveEmptyEntries;
            // カンマで 1 文字ごとに区切る
            var separator = new char[] { ',' };

            // 行ごとに分ける
            var lines = mapData.Split( new char[] { '\r', '\n' }, removeEmptyEntries );

            // マップの大きさの取得
            string[] size = lines[0].Split( separator, removeEmptyEntries );
            const int Width  = 0;
            const int Height = 1;
            mapWidth  = int.Parse( size[Width] );
            mapHeight = int.Parse( size[Height] );

            // マップデータの領域確保
            currentMapData = new int[mapHeight, mapWidth];

            for (int y = 0; y < mapHeight; y++)
            {
                // 列ごとに分ける
                var lineElements = lines[mapHeight - y].Split( separator, removeEmptyEntries );

                for (int x = 0; x < mapWidth; x++)
                {
                    if (lineElements.Length <= x)
                    {
                        break;
                    }

                    // マップデータの設定
                    currentMapData[y, x] = int.Parse( lineElements[x] );
                }
            }
        }

        //----------------------------------------------------------------------
        public int GetDataFromCurrentMap( Vector3 position )
        {
            return GetDataFromCurrentMap(
                Mathf.RoundToInt( position.x ),
                Mathf.RoundToInt( position.z )
            );
        }

        //----------------------------------------------------------------------
        public int GetDataFromCurrentMap( int gridXPosition, int gridZPosition )
        {
            const int OutOfRange = -1;

            if(gridXPosition >= mapWidth
                || gridZPosition >= mapHeight)
            {
                return OutOfRange;
            }

            return currentMapData[gridZPosition, gridXPosition];
        }

        //----------------------------------------------------------------------
        public Vector3 GetPlayerPosition()
        {
            return playerPosition;
        }

        //----------------------------------------------------------------------
        public Vector3 GetMonsterPosition()
        {
            return monsterPositions[Random.Range( 0, monsterPositions.Count )];
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// エサが取り除かれたことをマスタクライアントに通知する
        /// </summary>
        public void NotifyRemovingFood( Vector3 position )
        {
            // マスタクライアントに通知
            photonView.RPC(
                "RemoveFoodRPC",
                PhotonTargets.MasterClient,
                new object[] { position }
            );
        }

        //----------------------------------------------------------------------
        public void SpawnFood()
        {
            while (true)
            {
                var x = Random.Range( 0, mapWidth );
                var z = Random.Range( 0, mapHeight );

                var key = new Vector2( x, z );
                var position = new Vector3( x, 0, z );

                if (!foods.ContainsKey( key )
                    && GetDataFromCurrentMap( position ) != MapElements.Wall
                    && GetDataFromCurrentMap( position ) != MapElements.Monster)
                {
                    foods.Add( key, PhotonNetwork.Instantiate(
                        "Prefabs/Food",
                        position,
                        Quaternion.identity,
                        0
                    ) );
                    FoodCount++;
                    break;
                }
            }
        }
        #endregion
    }
}
