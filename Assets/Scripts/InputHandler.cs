﻿using UnityEngine;

namespace Maru
{
    public static class InputHandler
    {
        #region Types
        public delegate Vector3 Type();
        #endregion

        #region Constants
        const int NumDirectionChoices = 4;
        static readonly Vector3[] directionChoices = new Vector3[NumDirectionChoices]
        {
            Vector3.forward,
            Vector3.back,
            Vector3.right,
            Vector3.left
        };
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        public static Type GetPlayable()
        {
            return () =>
            {
                if (GameInput.Up)
                {
                    return Vector3.forward;
                }
                if (GameInput.Down)
                {
                    return Vector3.back;
                }
                if (GameInput.Right)
                {
                    return Vector3.right;
                }
                if (GameInput.Left)
                {
                    return Vector3.left;
                }

                // キー入力がない
                return Vector3.zero;
            };
        }

        //----------------------------------------------------------------------
        public static Type GetNonPlayable( Component target )
        {
            var gridBasedMover = target.GetComponent<GridBasedMover>();

            return () =>
            {
                var choice = Random.Range( 0, NumDirectionChoices );
                var primary = directionChoices[choice];
                var secondary = directionChoices[(choice + NumDirectionChoices / 2) % NumDirectionChoices];

                var direction = Vector3.zero;

                if (!gridBasedMover.IsBackwardDirection( primary )
                    && gridBasedMover.IsMovableTo( primary ))
                {
                    direction = primary;
                }
                else if (!gridBasedMover.IsBackwardDirection( secondary )
                    && gridBasedMover.IsMovableTo( secondary ))
                {
                    direction = secondary;
                }
                else if (!gridBasedMover.IsBackwardDirection( -primary )
                    && gridBasedMover.IsMovableTo( -primary ))
                {
                    direction = -primary;
                }
                else if (!gridBasedMover.IsBackwardDirection( -secondary )
                    && gridBasedMover.IsMovableTo( -secondary ))
                {
                    direction = -secondary;
                }

                if (direction == Vector3.zero)
                {
                    return gridBasedMover.Direction;
                }

                return direction;
            };
        }
        #endregion
    }
}
