﻿using UnityEngine;
using System.Collections;

namespace Maru
{
    /// <summary>
    /// マップの要素を定義するクラス
    /// </summary>
    public class MapElements
    {
        #region Constants
        public const int None = 0;
        public const int Wall = 1;
        public const int Player = 2;
        public const int Monster = 3;
        #endregion
    }
}
