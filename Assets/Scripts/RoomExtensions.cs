﻿using ExitGames.Client.Photon;

namespace Maru
{
    public static class RoomExtensions
    {
        #region Constants
        const string CurrentScoreKey  = "CurrentScore";
        const string HighScoreKey     = "HighScore";
        const string CurrentPlayerKey = "CurrentPlayerKey";
        const string HighPlayerKey    = "HighPlayerKey";
        const string InvincibleTime   = "InvincibleTime";
        #endregion

        #region Methods
        //----------------------------------------------------------------------
        public static void AddCurrentScore( this Room self, int value )
        {
            SetCurrentScore( self, GetCurrentScore( self ) + value );
        }

        //----------------------------------------------------------------------
        public static void SetCurrentScore( this Room self, int value )
        {
            var currentScore = new Hashtable();
            currentScore[CurrentScoreKey] = value;

            self.SetCustomProperties( currentScore );
        }

        //----------------------------------------------------------------------
        public static int GetCurrentScore( this Room self )
        {
            object value;
            if (self.customProperties.TryGetValue( CurrentScoreKey, out value ))
            {
                return (int)value;
            }

            return 0;
        }

        //----------------------------------------------------------------------
        public static void SetHighScore( this Room self, int value )
        {
            var highScore = new Hashtable();
            highScore[HighScoreKey] = value;

            self.SetCustomProperties( highScore );
        }

        //----------------------------------------------------------------------
        public static int GetHighScore( this Room self )
        {
            object value;
            if (self.customProperties.TryGetValue( HighScoreKey, out value ))
            {
                return (int)value;
            }

            return 0;
        }

        //----------------------------------------------------------------------
        public static void SetCurrentPlayer( this Room self, string value )
        {
            var currentPlayer = new Hashtable();
            currentPlayer[CurrentPlayerKey] = value;

            self.SetCustomProperties( currentPlayer );
        }

        //----------------------------------------------------------------------
        public static string GetCurrentPlayer( this Room self )
        {
            object value;
            if (self.customProperties.TryGetValue( CurrentPlayerKey, out value ))
            {
                return (string)value;
            }

            return string.Empty;
        }

        //----------------------------------------------------------------------
        public static void SetHighPlayer( this Room self, string value )
        {
            var highPlayer = new Hashtable();
            highPlayer[HighPlayerKey] = value;

            self.SetCustomProperties( highPlayer );
        }

        //----------------------------------------------------------------------
        public static string GetHighPlayer( this Room self )
        {
            object value;
            if (self.customProperties.TryGetValue( HighPlayerKey, out value ))
            {
                return (string)value;
            }

            return string.Empty;
        }

        //----------------------------------------------------------------------
        public static void SetInvincibleTime( this Room self, float value )
        {
            var invincibleTime = new Hashtable();
            invincibleTime[InvincibleTime] = value;

            self.SetCustomProperties( invincibleTime );
        }

        //----------------------------------------------------------------------
        public static float GetInvincibleTime( this Room self )
        {
            object value;
            if (self.customProperties.TryGetValue( InvincibleTime, out value ))
            {
                return (float)value;
            }

            return 0.0f;
        }
        #endregion
    }
}
