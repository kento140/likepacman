﻿using UnityEngine;

/// <summary>
/// 指定した Transform を追いかけるカメラ
/// </summary>
public class FollowCamera : MonoBehaviour
{
    #region Variables
    [SerializeField]
    Transform target = null;
    [SerializeField]
    Vector3 distanceToTarget = Vector3.back;
    #endregion

    #region Properties
    public Transform Target
    {
        get { return target; }
        set { target = value; }
    }
    #endregion

    #region Messages
    //----------------------------------------------------------------------
    void LateUpdate()
    {
        if (Target == null)
        {
            return;
        }

        transform.position = Target.position + distanceToTarget;
    }
    #endregion
}
