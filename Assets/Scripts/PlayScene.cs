﻿using PhotonRx;
using UniRx;
using UnityEngine;

namespace Maru
{
    [RequireComponent( typeof( PhotonView ) )]
    public class PlayScene : Photon.MonoBehaviour
    {
        #region Constants
        #endregion

        #region Variables
        #endregion

        #region Properties
        public bool Initialized { get; private set; }
        #endregion

        #region Messages
        //----------------------------------------------------------------------
        void Awake()
        {
            Debug.Log( string.Format( "シーン変更完了: {0}", PhotonNetwork.player.ID ) );

            // イベントの送受信を再開
            PhotonNetwork.isMessageQueueRunning = true;

            // ルームの既存プレイヤは初期化済み？
            object initialized;
            if (PhotonNetwork.room.customProperties.TryGetValue( "Initialized", out initialized )
                && (bool)initialized)
            {
                // 既存プレイヤに続いて初期化する
                Initialize();
                StartGamePlay();

                return;
            }

            if (PhotonNetwork.isMasterClient)
            {
                // プレイヤのカスタムプロパティが変更されたことを通知するストリーム
                this.OnPhotonPlayerPropertiesChangedAsObservable()
                    // 全員がシーンを変更し終えたとき
                    .Buffer( PhotonNetwork.playerList.Length )
                    // 1 度だけ
                    .First()
                    // 初期化する
                    .Subscribe( _ =>
                    {
                        // シーン遷移が完了したことをルームに保存しておく
                        PhotonNetwork.room.SetCustomProperties( new ExitGames.Client.Photon.Hashtable() { { "Loaded", true } } );

                        // マップの選択 (生成はまだしない)
                        int mapFileIndex = MapManager.Instance.ChooseMap();
                        // マップの識別子をルームに保存しておく
                        PhotonNetwork.room.SetCustomProperties( new ExitGames.Client.Photon.Hashtable() { { "MapFileIndex", mapFileIndex } } );

                        // 全クライアントの初期化関数を呼び出す
                        photonView.RPC( "Initialize", PhotonTargets.All );
                    } );
            }

            // 自身のシーン変更完了を通知
            var loaded = new ExitGames.Client.Photon.Hashtable() { { "Loaded", true } };
            PhotonNetwork.SetPlayerCustomProperties( loaded );
        }

        //----------------------------------------------------------------------
        void Update()
        {
            if (!Initialized)
            {
                return;
            }

            if (!PhotonNetwork.isMasterClient)
            {
                return;
            }

            // エサの出現
            int minFoodCount = PhotonNetwork.playerList.Length * PhotonNetwork.playerList.Length + 10;
            if (MapManager.Instance.FoodCount < minFoodCount)
            {
                MapManager.Instance.SpawnFood();
            }

            PhotonNetwork.room.SetInvincibleTime( PhotonNetwork.room.GetInvincibleTime() - Time.deltaTime );
        }

        //----------------------------------------------------------------------
        void OnGUI()
        {
            // 各プレイヤ名
            GUI.contentColor = Color.green;

            foreach (var player in PhotonNetwork.playerList)
            {
                var screenPosition = Camera.main.WorldToScreenPoint( player.GetPosition() );

                const float FontWidth = 7.0f;
                float offsetX = player.name.Length * -FontWidth;
                const float offsetY = 40.0f; // 少し上

                GUI.Label(
                    new Rect(
                        screenPosition.x + offsetX,
                        Camera.main.pixelHeight - (screenPosition.y + offsetY),
                        200,
                        100
                    ),
                    player.name
                );
            }

            GUI.contentColor = Color.cyan;

            GUI.Label(
                new Rect( 0, 0, 100, 50 ),
                string.Format( "現在 {0} 人", PhotonNetwork.playerList.Length )
            );

            var room = PhotonNetwork.room;
            // 現在のスコア
            GUI.Label(
                new Rect( 100, 0, 150, 20 ),
                room.GetCurrentPlayer()
            );
            GUI.Label(
                new Rect( 100, 20, 150, 20 ),
                string.Format( "SCORE: {0}", room.GetCurrentScore() )
            );
            // ハイスコア
             GUI.Label(
                new Rect( Screen.width / 2, 0, 150, 20 ),
                room.GetHighPlayer()
            );
            GUI.Label(
                new Rect( Screen.width / 2, 20, 150, 20 ),
                string.Format("HISCORE: {0}", room.GetHighScore() )
            );
        }

        //----------------------------------------------------------------------
        //void OnDestroy()
        //{
        //    var resetProperties = new ExitGames.Client.Photon.Hashtable() {
        //        { "Loaded", false },
        //        { "Initialized", false }
        //    };
            
        //    PhotonNetwork.SetPlayerCustomProperties( resetProperties );
        //}
        #endregion

        #region RPC
        //----------------------------------------------------------------------
        [PunRPC]
        void Initialize()
        {
            Debug.Log( string.Format( "Initialize(): {0}", PhotonNetwork.player.ID ) );

            if (PhotonNetwork.isMasterClient)
            {
                // プレイヤのカスタムプロパティが変更されたことを通知するストリーム
                this.OnPhotonPlayerPropertiesChangedAsObservable()
                    // 全員が初期化をし終えたとき
                    .Buffer( PhotonNetwork.playerList.Length )
                    // 1 度だけ
                    .First()
                    // ゲームプレイを開始する
                    .Subscribe( _ =>
                    {
                        // 初期化完了したことをルームに保存しておく
                        PhotonNetwork.room.SetCustomProperties( new ExitGames.Client.Photon.Hashtable() { { "Initialized", true } } );

                        // 新規の入室を許可する
                        PhotonNetwork.room.open = true;

                        // 全クライアントのゲームプレイを開始
                        photonView.RPC( "StartGamePlay", PhotonTargets.All );
                    } );
            }

            // 初期化の実装部分
            InitializeImpl();

            // 自身の初期化完了を通知
            var initialized = new ExitGames.Client.Photon.Hashtable() { { "Initialized", true } };
            PhotonNetwork.SetPlayerCustomProperties( initialized );
        }

        //----------------------------------------------------------------------
        [PunRPC]
        void StartGamePlay()
        {
            Debug.Log( string.Format( "StartGamePlay(): {0}", PhotonNetwork.player.ID ) );

            Initialized = true;
            Time.timeScale = 1.0f;
        }
#endregion

#region Methods
        //----------------------------------------------------------------------
        void InitializeImpl()
        {
            // 共通の初期化処理 ...
            Time.timeScale = 0.0f;

            // ... マップの生成
            MapManager.Instance.LoadMapFromFile( (int)PhotonNetwork.room.customProperties["MapFileIndex"] );
            MapManager.Instance.CreateLoadedMap();

            // マスタか否かに依存する初期化処理
            InitializeIfMasterClient();
            InitializeIfNonMasterClient();
        }

        //----------------------------------------------------------------------
        void InitializeIfMasterClient()
        {
            if (!PhotonNetwork.isMasterClient)
            {
                return;
            }

            // プレイヤの生成
            PhotonNetwork.player.IsPlayer( true );
            var player = PhotonNetwork.Instantiate(
                "Prefabs/Player",
                MapManager.Instance.GetPlayerPosition(),
                Quaternion.identity,
                0,
                null
            );
            PlayerController.CreatePlayablePlayer( player.GetComponent<PlayerController>() );
            PhotonNetwork.room.SetCurrentPlayer( PhotonNetwork.player.name );

            // カメラの追跡対象を操作するプレイヤに設定
            Camera.main.GetComponent<FollowCamera>().Target = player.transform;

            // ノンプレイアブルなモンスターの生成
            //const int ForPlayer = 1;
            //int numPlayableMonsters = PhotonNetwork.playerList.Length - ForPlayer;

            //int numNonPlayableMonsters = MapManager.Instance.MonsterCount - numPlayableMonsters;
            //for (int i = 0; i < numNonPlayableMonsters; i++)
            //{
            //    var monster = PhotonNetwork.InstantiateSceneObject(
            //        "Prefabs/Monster",
            //        MapManager.Instance.GetMonsterPosition(),
            //        Quaternion.identity,
            //        0,
            //        null
            //    );
            //    MonsterController.CreateNonPlayableMonster( monster.GetComponent<MonsterController>() );
            //}
        }

        //----------------------------------------------------------------------
        void InitializeIfNonMasterClient()
        {
            if (PhotonNetwork.isMasterClient)
            {
                return;
            }

            // プレイアブルなモンスターの生成
            var monster = PhotonNetwork.Instantiate(
                "Prefabs/Monster",
                MapManager.Instance.GetMonsterPosition(),
                Quaternion.identity,
                0
            );
            MonsterController.CreatePlayableMonster( monster.GetComponent<MonsterController>() );

            // カメラの追跡対象を操作するモンスターに設定
            Camera.main.GetComponent<FollowCamera>().Target = monster.transform;
        }
#endregion
    }
}
