﻿using UnityEngine;
using System.Collections;

namespace Maru
{
    public class Score : MonoBehaviour
    {
        public void Increment()
        {
            PhotonNetwork.room.AddCurrentScore( 1 );
        }
    }
}
